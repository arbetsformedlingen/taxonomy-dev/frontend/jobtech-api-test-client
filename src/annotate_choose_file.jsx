import React from 'react';
import Rest from './rest.jsx';
import Button from './button.jsx';

class AnnotateChooseFile extends React.Component { 

	constructor() {
        super();
        // state
        this.state = {
			text: "",			
        };
		this.firstSelected = null;
        this.boundGlobalDragEnter = this.onGlobalDragEnter.bind(this);
        this.boundGlobalDragOver = this.onGlobalDragOver.bind(this);
        this.boundGlobalDrop = this.onGlobalDrop.bind(this);
    }

    componentDidMount() {
        this.enableFileDrag();
    }

    componentWillUnmount() {
        this.disableFileDrag();
    }

	enableFileDrag() {
        window.addEventListener("dragenter", this.boundGlobalDragEnter, false);
        window.addEventListener("dragover", this.boundGlobalDragOver);
        window.addEventListener("drop", this.boundGlobalDrop);
    }

    disableFileDrag() {
        window.removeEventListener("dragenter", this.boundGlobalDragEnter);
        window.removeEventListener("dragover", this.boundGlobalDragOver);
        window.removeEventListener("drop", this.boundGlobalDrop);
    }

	sendFile(file) {
		this.setState({loading: true}, () => {
			Rest.postFileUpload(file, (data) => {
				this.setState({data: data, loading: false});
			}, (status) => {
				this.setState({loading: false});
			});
		});
	}

	onTextChanged(e) {
		var text = e.target.value;
		this.setState({text: text});
	}

	onGlobalDragEnter(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onGlobalDragOver(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onGlobalDrop(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onDragEnter(e) {
        e.preventDefault();
    }

    onDragOver(e) {
        e.preventDefault();
    }

    onDrop(e) {
        e.preventDefault();
        var file = e.dataTransfer.files[0];
        this.props.onFromFile(file);
    }

    onFilePicker(e) {
        var file = e.target.files[0];      
        this.props.onFromFile(file);
    }

    onChooseFileClicked() {
        var filePicker = document.getElementById("file_picker");
        filePicker.click();
    }

	onFromTextClicked() {
		if(this.state.text.length > 0) {
			this.props.onFromText(this.state.text);
		}
	}

    render() {
        return (
			<div>
				<div>Från text</div>
				<div className="search">
					<textarea 
						className="rounded"
						type="text"
						value={this.state.text}
						placeholder="Text att annotera"
						onChange={this.onTextChanged.bind(this)}
					/>
				</div>
				<div className="button_row">
					<Button
						onClick={this.onFromTextClicked.bind(this)}
						text="Kör"/>
				</div>
				<div>
					Från fil
					<div
						className="choose_import_drag_drop"
						id="drag_drop"
						accept=".pdf"
						onDragEnter={this.onDragEnter.bind(this)}
						onDragOver={this.onDragOver.bind(this)}
						onDrop={this.onDrop.bind(this)}>
							<div 
								className="choose_import_dropzone"
								id="dropzone">
								Dra och släpp här
							</div> 
					</div>
					<div className="button_row">
						<Button
							onClick={this.onChooseFileClicked.bind(this)}
							text="Välj fil"/>							
					</div>
					<input 
						id="file_picker" 
						className="choose_import_button_file_picker" 
						type="file"
						accept=".pdf"
						onChange={this.onFilePicker.bind(this)} />
				</div>
			</div>
        );
    }
	
}

export default AnnotateChooseFile