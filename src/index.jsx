import React from 'react';
import ReactDOM from 'react-dom';
import Annotate from './annotate.jsx';
import Recommender from './recommender.jsx';

class Index extends React.Component { 

	constructor() {
        super();
		this.RECOMMENDER = "RECOMMENDER";
		this.ANNOTATE = "ANNOTATE";
        // state
        this.state = {
			page: this.RECOMMENDER,
        };
        // callbacks
    }

    componentDidMount() {
    }

	onMenuSelect(page) {
		this.setState({page: page});
	}

	renderMenu() {
		var menuAlternative = (page, text) => { 
			if (page == this.state.page) {
				return (<div className="selected" onPointerUp={this.onMenuSelect.bind(this, page)}>{text}</div>);
			} else {
				return (<div onPointerUp={this.onMenuSelect.bind(this, page)}>{text}</div>);
			}
		};
		return (
			<div className="nav-menu">
				{menuAlternative(this.RECOMMENDER, "Recommender")}
				{menuAlternative(this.ANNOTATE, "Annotate")}
			</div>
		);

	}
	
	renderContent() {
		switch(this.state.page) {
			case this.ANNOTATE:
				return (<Annotate/>);
			case this.RECOMMENDER:
			default:
				return (<Recommender/>);
		}
	}

    render() {
        return (
            <div className="main">
				{this.renderMenu()}
				{this.renderContent()}
			</div>
        );
    }
	
}

ReactDOM.render(<Index/>, document.getElementById('content'));