import React from 'react';

class Constants { 

    constructor() {
        // settings
        //this.REST_IP = "http://localhost:8010/proxy";
		this.REST_IP = "http://mentor-api-mentor-api-4.test.services.jtech.se";
        this.REST_API_KEY = "111";
    }

}

export default new Constants;