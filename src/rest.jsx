import React from 'react';
import Constants from './constants.jsx';

class Rest {

    setupCallbacks(http, onSuccess, onError) {
        this.currentRequest = http;
        http.onerror = () => {
            if(onError != null) {
                onError(http.status);
            }
        }
        http.onload = () => {
            if(http.status >= 200 && http.status < 300) {
                if(onSuccess != null) {
                    try {
                        var response = http.response.split("\"taxonomy/").join("\"");
                        response = response.split("preferred-label").join("preferredLabel");
                        onSuccess(JSON.parse(response));
                    } catch(err) {
                        console.log("Exception", err);
                    }
                }
            } else {
                if(onError != null) {
                    onError(http.status);
                }
            }
        }
    }

    abort() {
        if(this.currentRequest) {
            this.currentRequest.abort();
        }
        if(this.currentErrorCallback) {
            this.currentErrorCallback(499); //Client Closed Request
        }
        this.currentRequest = null;
    }

    get(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    post(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    postBody(func, body, contentType, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        if(contentType) {
            http.setRequestHeader("Content-Type", contentType);
        }
        http.send(body);
    }

    postBody2(func, body, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send(body);
    }

    patch(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("PATCH", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    delete(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("DELETE", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    getPromise(func) {
		return new Promise((resolve, reject) => {
			var http = new XMLHttpRequest();
			http.onerror = () => {
				reject(http.status);
			}
			http.onload = () => {
				if(http.status >= 200 && http.status < 300) {
					try {
						var response = http.response.split("\"taxonomy/").join("\"");
						response = response.split("preferred-label").join("preferredLabel");
						response = JSON.parse(response);
						resolve(response);
					} catch(err) {
						console.log("Exception", err);
					}
				} else {
					reject(http.status);
				}
			}
			http.open("GET", Constants.REST_IP + func, true);
			http.setRequestHeader("api-key", Constants.REST_API_KEY);
			http.setRequestHeader("Accept", "application/json");
			http.send();
		});
    }

    getVersionsPromis() {
		return this.getPromise("/main/versions");
    }

	getConcept(id, onSuccess, onError) {
		this.abort();
		var q = "query MyQuery { concepts(id: \"" + id + "\") { related(type: \"skill\") { id type preferredLabel:preferred_label } } }";
		var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", "https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql?query=" + q, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();		
	}

	autocomplete(text, onSuccess, onError) {
		this.abort();
		var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", "https://taxonomy.api.jobtechdev.se/v1/taxonomy/suggesters/autocomplete?query-string=" + text, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();		
	}

    getTypes(onSuccess, onError) {
		this.abort();
		var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", "https://taxonomy.api.jobtechdev.se/v1/taxonomy/main/concept/types", true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();		
	}

	recommender(ids, onSuccess, onError) {
		this.abort();
		this.get("/main/recommender?type=skill&version=2&id=" + ids, onSuccess, onError);
	}

    postEducationDescription(text, onSuccess, onError) {
        this.abort();
        this.post("/nlp/education-description?text=" + encodeURIComponent(text), onSuccess, onError);
    }

    postFileUpload(file, onSuccess, onError) {
        this.abort();
        var fd = new FormData();
        fd.append("file", file, file.name);
        this.postBody("/files/upload-education-description", fd, null, onSuccess, onError);
    }

    saveAnnotatedText(annotatedDoc, onSuccess, onError) {
        this.abort();
        annotatedDoc = annotatedDoc.split("preferredLabel").join("preferred-label");
        this.postBody("/private/save-annotated-text", annotatedDoc, "application/json", onSuccess, onError);
    }
}

export default new Rest;