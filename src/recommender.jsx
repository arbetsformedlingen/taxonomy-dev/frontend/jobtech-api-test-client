import React from 'react';
import ReactDOM from 'react-dom';
import Rest from './rest.jsx';

class Recommender extends React.Component { 

	constructor() {
        super();
        // state
        this.state = {
			text: "",
			data: [],
			selectedItems: [],
			suggestedItems: [],
			selectedKeywords: [],
			suggestedKeywords: [],
			selectedOccupations: [],
			suggestedOccupations: [],
        };
        // callbacks
    }

    componentDidMount() {
    }
	
	onTextChanged(e) {
		var text = e.target.value;
		this.setState({text: text});
		//start to fetch suggestions
		if(text.length > 1) {
			Rest.autocomplete(text, (data) => {
				data = data.filter((item) => {
					return item.type == "skill" || item.type == "ssyk-level-4" || item.type == "occupation-name" || item.type == "keyword";
				});
				this.setState({data: data});
			}, (status) => {
				console.log("Error: " + status)
			});
		} else {
			this.setState({data: []});
		}
	}
	
	onSelectItemInDropdown(item) {
		if(item.type == "skill" || item.type == "keyword") {
			this.state.selectedKeywords.push(item);
			this.setState({text: "", data: [],});
			this.getSuggestions();			
		} else {
			this.state.selectedOccupations.push(item);
			this.setState({text: "", data: [],});
			this.getSuggestions();
			console.log(item);
			if(item.type == "ssyk-level-4") {
				//fetch concept and add related skill automatically
				Rest.getConcept(item.id, (data) => {
					console.log(data);
					var concept = data.data.concepts[0];
					if(concept.related.length > 0) {
						this.state.selectedKeywords.push(...concept.related);
						this.getSuggestions();
					}
				}, (status) => {
					console.log("Error: " + status)
				});
			}
		}
	}
	
	onClickSelectedKeyword(item, i) {
		this.state.selectedKeywords.splice(i, 1);
		this.getSuggestions();
	}
	
	onClickSuggestedKeyword(item, i) {
		console.log(item);
		this.state.selectedKeywords.push(item);
		this.getSuggestions();
	}

	onClickSelectedOccupation(item, i) {
		this.state.selectedOccupations.splice(i, 1);
		this.getSuggestions();
	}
	
	onClickSuggestedOccupation(item, i) {
		console.log(item);
		this.state.selectedOccupations.push(item);
		this.getSuggestions();
	}
	
	filterDuplicates(data, items) {
		return data.filter((i) => {
			return items.filter((item) => {
				return i.id == item.id;
			}).length == 0;
		});
	}
	
	filterType(data, type) {
		return data.filter((item) => { return item.type == type; }); 
	};
	
	getSuggestions() {
		this.getKeywordSuggestions();
		this.getOccupationSuggestions();
	}
	
	getKeywordSuggestions() {
		if(this.state.selectedKeywords.length > 0) {
			var ids = this.state.selectedKeywords[this.state.selectedKeywords.length - 1].id;
			//TODO funkar ej att lämna hel lista ännu
			/*for(var i=1; i<this.state.selectedKeywords.length; ++i) {
				ids += "," + this.state.selectedKeywords[i].id;
			}*/
			Rest.recommender(ids, (data) => {				
				this.setState({suggestedKeywords: this.filterDuplicates(data.concepts, this.state.selectedKeywords)});
			}, (status) => {
				console.log("Error: " + status);
			});
		} else {
			this.setState({suggestedKeywords: [],});
		}
	}
	
	getOccupationSuggestions() {
		if(this.state.selectedOccupations.length > 0) {
			var ids = this.state.selectedOccupations[this.state.selectedOccupations.length - 1].id;
			//TODO funkar ej att lämna hel lista ännu
			/*for(var i=1; i<this.state.selectedOccupations.length; ++i) {
				ids += "," + this.state.selectedOccupations[i].id;
			}*/
			Rest.recommender(ids, (data) => {				
				this.setState({suggestedOccupations: this.filterDuplicates(data.concepts, this.state.selectedOccupations)});
			}, (status) => {
				console.log("Error: " + status);
			});
		} else {
			this.setState({suggestedOccupations: [],});
		}
	}
	
	renderDropdown() {		
		var htmlFor = (arr, css, title) => {
			var items = arr.map((item, i) => {
				return (
					<div className={css}
						key={i}
						onClick={this.onSelectItemInDropdown.bind(this, item)}>
						{item.preferredLabel}
					</div>
				);
			});
			if(items.length > 0) {
				items.unshift(<div key={items.length} className="title">{title}</div>);
			}
			return items;
		};
		var skillData = this.filterType(this.state.data, "skill");
		var ssykData = this.filterType(this.state.data, "ssyk-level-4");
		var occupationData = this.filterType(this.state.data, "occupation-name");
		var keywordData = this.filterType(this.state.data, "keyword");
		if(skillData.length > 0 || ssykData.length > 0 || occupationData.length > 0 || keywordData.length > 0) {
			var skills = htmlFor(skillData, "skill", "Kompetens");
			var ssyks = htmlFor(ssykData, "ssyk", "SSYK");
			var occupations = htmlFor(occupationData, "occupation", "Yrkesbenämning");
			var keywords = htmlFor(keywordData, "keyword", "Sökbegrepp");
			return (
				<div className="dropdown-content">
					{skills}
					{ssyks}
					{occupations}
					{keywords}
				</div>
			);
		}
	}
	
	renderSelectedItems() {
		var items = this.state.selectedItems.map((item, i) => {
			return (
				<div
					key={i} 
					onClick={this.onClickSelectedItem.bind(this, item, i)}>
					{item.preferredLabel}
				</div>	
			);
		});
		return (
			<div>				
				<div className="selected-items">
				{items}
				</div>
			</div>
		);
	}
	
	renderSuggestedItems() {
		var items = this.state.suggestedItems.map((item, i) => {
			return (
				<div
					key={i} 
					onClick={this.onClickSuggestedItem.bind(this, item, i)}>
					{item.preferredLabel}
				</div>	
			);
		});
		return (
			<div className="suggested-items">
			{items}
			</div>
		);
	}
	
	renderSelectedKeywords() {
		var items = this.state.selectedKeywords.map((item, i) => {
			return (
				<div
					key={i} 
					onClick={this.onClickSelectedKeyword.bind(this, item, i)}>
					{item.preferredLabel}
				</div>	
			);
		});
		return (
			<div>				
				<div className="selected-items">
				{items}
				</div>
			</div>
		);
	}

	renderSuggestedKeywords() {
		var items = this.state.suggestedKeywords.map((item, i) => {
			return (
				<div
					key={i}
					onClick={this.onClickSuggestedKeyword.bind(this, item, i)}>
					{item.preferredLabel}
				</div>
			);
		});
		return (
			<div className="suggested-items">
			{items}
			</div>
		);
	}

	renderSelectedOccupations() {
		var items = this.state.selectedOccupations.map((item, i) => {
			return (
				<div
					key={i}
					onClick={this.onClickSelectedOccupation.bind(this, item, i)}>
					{item.preferredLabel}
				</div>
			);
		});
		return (
			<div>
				<div className="selected-items">
				{items}
				</div>
			</div>
		);
	}

	renderSuggestedOccupations() {
		var items = this.state.suggestedOccupations.map((item, i) => {
			return (
				<div
					key={i}
					onClick={this.onClickSuggestedOccupation.bind(this, item, i)}>
					{item.preferredLabel}
				</div>
			);
		});
		return (
			<div className="suggested-items">
			{items}
			</div>
		);
	}

    render() {
        return (
			<div className="content">
				<h1>Mina kunskaper & yrken</h1>
				<div className="search">
					<input 
						className="rounded"
						type="text"
						value={this.state.text}
						placeholder="Börja skriva en kunskap eller ett yrke"
						onChange={this.onTextChanged.bind(this)}
					/>
					{this.renderDropdown()}
				</div>
				<div className="items">
					<div>
						{this.state.selectedOccupations.length > 0 &&
							<div>Mina yrken</div>
						}
						{this.renderSelectedOccupations()}
						{this.state.suggestedOccupations.length > 0 &&
							<div>Förslag</div>
						}
						{this.renderSuggestedOccupations()}
					</div>					
					<div>
						{this.state.selectedKeywords.length > 0 &&
							<div>Mina kunskaper</div>
						}
						{this.renderSelectedKeywords()}
						{this.state.suggestedKeywords.length > 0 &&
							<div>Förslag</div>
						}
						{this.renderSuggestedKeywords()}
					</div>
				</div>
			</div>
        );
    }
	
}

export default Recommender