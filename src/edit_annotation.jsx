import React from 'react';
import ReactDOM from 'react-dom';
import Rest from './rest.jsx';
import Button from './button.jsx';

class EditAnnotation extends React.Component { 

	constructor() {
        super();
        // state
        this.state = {
			searchFor: "",
			searchForResult: null,
        };
		this.firstSelected = null;
    }

	onSearchForChanged(e) {
		var text = e.target.value;
		this.setState({searchFor: text});
		if(text.length > 1) {
			Rest.autocomplete(text, (data) => {				
				this.setState({searchForResult: data});
			}, (status) => {
				console.log("Error: " + status)
			});
		} else {
			this.setState({searchForResult: null});
		}
	}

	onChangeAnnotationConnectedTo() {
		this.setState({editConnection: true});
	}

	onSaveNewAnnotationClick() {
		this.props.onAnnotationSaved();
	}

	onDeleteAnnotationClick() {
		this.props.onAnnotationDeleted();
	}

	onSelectAnnotationConcept(concept) {
		if(this.props.annotation) {
			this.props.annotation["concept-id"] = concept.id;
			this.props.annotation["preferredLabel"] = concept.preferredLabel;
			this.props.annotation["type"] = concept.type;
		}
		this.setState({
			editConnection: false,
			searchFor: "",
			searchForResult: null,});
	}

	onTypeChanged(e) {		
		if(this.props.annotation) {
			this.props.annotation["concept-id"] = null;
			this.props.annotation["preferredLabel"] = null;
			this.props.annotation["type"] = e.target.value;
		}
		this.setState({
			editConnection: false,
			searchFor: "",
			searchForResult: null,});
	}

	renderDropdown() {
		if(this.state.searchForResult) {
			var items = this.state.searchForResult.map((item, i) => {
				return(
					<div
						onPointerUp={this.onSelectAnnotationConcept.bind(this, item)} 
						className="dropdown-item" 
						key={i}>
						<div>{item.type}</div>
						<div>{item.preferredLabel}</div>
					</div>
				);
			});
			return(
				<div className="dropdown-content">
					{items}
				</div>
			);
		}
	}

	renderOptions() {
		var types = this.props.annotationTypes.map((t, i) => {
			return (
				<option 
					value={t} 
					key={i}>
					{t}
				</option>
			);
		});
		types.unshift(<option key={this.props.annotationTypes.lenght + 1} disabled value=""> -- välj en typ -- </option>);
		return types;
	}

	renderDialogConnected() {
		if(this.state.editConnection) {
			return (
				<div className="connected_to dialog_group">
					<div className="title">Kopplad mot</div>
					<div>
						Sök koncept:
					</div>
					<div className="input_holder">
						<input 
							className="rounded"
							type="text"
							value={this.state.searchFor}
							placeholder="Sök koncept"
							onChange={this.onSearchForChanged.bind(this)}
						/>
					</div>
					<div className="dropdown">
						{this.renderDropdown()}
					</div>
					<div>
						Välj typ:
					</div>
					<select
						className="rounded"
						value=""
						onChange={this.onTypeChanged.bind(this)}>
						{this.renderOptions()}
					</select>
				</div>
			);			
		}
		return (
			<div className="dialog_group connected_to">
				<div className="title">Kopplad mot</div>
				{this.renderField("Typ", this.props.annotation.type)}				
				{this.props.annotation["concept-id"] &&
					this.renderField("Konceptid", this.props.annotation["concept-id"])
				}
				{this.props.annotation.preferredLabel &&
					this.renderField("Konceptname", this.props.annotation.preferredLabel)
				}
				<div className="button_row">
					<Button
						css="edit"
						onClick={this.onChangeAnnotationConnectedTo.bind(this)}
						text="Ändra"/>					
				</div>
			</div>
		);

	}

	renderField(name, val) {
		return (
			<div className="annotation_field">
				<div>{name}</div>
				<div>{val}</div>
			</div>
		);
	};

    render() {
        return (
			<div>
				<div className="title">Edit</div>
				<div className="dialog_content">
					<div className="dialog_group">
						<div className="title">Info</div>
						{this.renderField("Id", this.props.annotation["annotation-id"])}
						{this.renderField("Matched string", this.props.annotation["matched-string"])}
					</div>
					{this.renderDialogConnected()}
				</div>
				<div className="button_row">
					{this.props.newConnection == true &&
					<Button
						css="save"
						isEnabled={this.props.annotation.type ? true : false}
						onClick={this.onSaveNewAnnotationClick.bind(this)}
						text="Spara"/>
					}
					{this.props.newConnection != true &&
					<Button
						css="remove"
						onClick={this.onDeleteAnnotationClick.bind(this)}
						text="Ta bort"/>
					}
					<Button
						onClick={this.props.onAnnotationChanged.bind(this, null)}
						text={this.props.newConnection == true ? "Avbryt" : "Stäng"}/>
				</div>
			</div>
        );
    }
	
}

export default EditAnnotation