import React from 'react';
import Rest from './rest.jsx';
import Button from './button.jsx';
import AnnotateChooseFile from './annotate_choose_file.jsx';
import AnnotateLoading from './annotate_loading.jsx';
import EditAnnotation from './edit_annotation.jsx';

class Annotate extends React.Component { 

	constructor() {
        super();
        // state
        this.state = {
			data: null,
			selected: null,
			loading: false,
			selectedTypes: [],
			selectedText: null,
			selectedId: null,
			filter: null,
			isGroupingOnType: false,
        };
		this.firstSelected = null;        
		this.boundSelectionChange = this.onSelectionChange.bind(this);
		this.types = [];
    }

    componentDidMount() {        
		document.addEventListener("selectionchange", this.boundSelectionChange);
		Rest.getTypes((data) => {
			this.types = data;
		}, (status) => {
			console.log("Error fetching types", status);
		});
    }

    componentWillUnmount() {        
		document.removeEventListener("selectionChange", this.boundSelectionChange);
    }

	sendFile(file) {
		this.setState({loading: true}, () => {
			Rest.postFileUpload(file, (data) => {
				this.setState({data: data, loading: false});
			}, (status) => {
				this.setState({loading: false});
			});
		});
	}

	onFilterChanged(e) {
		this.setState({filter: e.target.value});
	}

	onGroupOnTypeChanged(e) {
		this.setState({
			selectedId: null,
			isGroupingOnType: e.target.checked,
		});
	}

	onSelectionChange(e) {		
		var selection = window.getSelection();		
		if(selection.anchorNode != null && selection.anchorNode == selection.focusNode) {
			var element = selection.anchorNode.parentElement;
			if(element.id && element.id.startsWith("a_")) {
				if(selection.anchorOffset != selection.focusOffset) {
					var id = element.id.substring(2);
					var annotations = this.state.data.annotations.filter((a) => {
						return a["annotation-id"] == id;
					});					
					if(annotations.length > 0 || id == "first") {						
						var from = selection.anchorOffset;
						var to = selection.focusOffset;
						if(annotations.length > 0) {
							from += annotations[0]["end-position"];
							to += annotations[0]["end-position"];
						}
						if(from > to) {
							var tmp = from;
							from = to;
							to = tmp;
						}
						var text = selection.anchorNode.nodeValue.substring(selection.anchorOffset, selection.focusOffset);
						for(var pos=0; pos < text.length; ++pos) {
							if(text[pos] != ' ' && text[pos] != '\t' && text[pos] != '\n') {
								if(pos > 0) {
									text = text.substring(pos);
									from += pos;									
								}
								break;
							}
						}
						for(var pos=0; pos < text.length; ++pos) {
							var c = text[text.length - 1 - pos];
							if(c != ' ' && c != '\t' && c != '\n') {
								text = text.substring(0, text.length - pos);
								to -= pos;								
								break;
							}
						}
						if(text.length > 0) {
							this.setState({selectedText: {
								text: text,
								from: from,
								to: to,
							}});
							return;
						}
					}
				}
				if(this.state.selectedText) {
					this.setState({selectedText: null});
				}
			} else if(element.id && element.id.startsWith("annotation_")) {
				if(this.state.selectedText) {
					this.setState({selectedText: null});
				}
			}
		} else if(selection.anchorNode != null && this.state.selectedText) {
			this.setState({selectedText: null});
		}
	}	

	onFromText(text) {
		this.setState({loading: true}, () => {
			Rest.postEducationDescription(text, (data) => {
				this.setState({data: data, loading: false});
			}, (status) => {
				this.setState({loading: false});
			});
		});
	}

	onFromFile(file) {
		this.sendFile(file);
	}

	onCreateAnnotationClicked(selection) {
		var nextId = ()=>{
			var id = 0;
			this.state.data.annotations.forEach((a) => {
				if(a["annotation-id"] > id) {
					id = a["annotation-id"];
				}				
			});
			return id + 1;
		};
		if(selection) {
			var text = this.state.data.text.substring(selection.from, selection.to);
			var id = nextId();
			this.setState({selected: {
					"annotation-id": id,
					"concept-id": null,
					"end-position": selection.to,
					"matched-string": text,
					preferredLabel: text,
					"start-position": selection.from,
					type: null,
				},
				editConnection: true,
				newConnection: true,
				selectedText: null,});
		}		
	}

	onSaveNewAnnotationClick() {
		this.state.data.annotations.push(this.state.selected);
		this.setState({selected: null});
	}

	onDeleteAnnotationClick() {
		var i = this.state.data.annotations.indexOf(this.state.selected);
		if(i >= 0) {
			this.state.data.annotations.splice(i, 1);
		}
		this.setState({selected: null});
	}

	onSelectAnnotationClick(annotation) {
		this.setState({
			selected: annotation,
			newConnection: false,
		});
	}

	onToggleTypeSelectedClicked(type) {
		var pos = this.state.selectedTypes.findIndex((a)=>{return a == type;});
		if(pos >= 0) {
			this.state.selectedTypes.splice(pos, 1);
		} else {
			this.state.selectedTypes.push(type);
		}
		this.forceUpdate(() => {
			setTimeout(()=>{
				if(this.firstSelected) {
					var element = document.getElementById("annotation_" + this.firstSelected["annotation-id"]);				
					if(element) {
						element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
					}
				}
			}, 500);
		});
	}

	onAnnotationClicked(annotation) {
		this.setState({
			selectedId: this.state.selectedId == annotation["annotation-id"] ? null : annotation["annotation-id"],
		}, () => {
			if(this.state.selectedId != null) {
				var element = document.getElementById("annotation_" + this.state.selectedId);
				if(element) {
					element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
				}
			}
		});	
	}

	onAnnotationSaved() {
		this.onSaveNewAnnotationClick();
	}

	onAnnotationChanged() {
		this.setState({selected: null});
	}

	onAnnotationDeleted() {
		this.onDeleteAnnotationClick();
	}

	onHideDialog() {
		this.setState({selected: null});
	}

	onSaveClicked() {
		var saveObj = {
			"annotated-doc": this.state.data,
			"api-key": "EnHamsterÄrEnGodVänIEnsammaStunder33!"
		};
		console.log(saveObj);
		Rest.saveAnnotatedText(JSON.stringify(saveObj), (data)=>{
			console.log("Annotated text saved");
			this.setState({
				data: null,
				loading: false
			});
		}, (status)=>{console.log("failed to save document", status)});
	}

	buildText() {
		var isSelected = (item) => {
			return this.state.selectedId == null || this.state.selectedId == item["annotation-id"];
		};
		/*var isTypeSelected = (type) => {
			if(this.state.selectedTypes.length > 0) {
				return this.state.selectedTypes.includes(type);
			} else {
				return true;
			}
		};*/
		this.firstSelected = null;
		var res = [];
		var annotations = this.state.data.annotations.sort((a, b) => { return a["start-position"] - b["start-position"]; });
		var text = this.state.data.text;
		var key = 0;
		var lastEnd = 0;
		var lastId = "first";
		for(var i=0; i<annotations.length; ++i) {
			var a = annotations[i];
			if(lastEnd <= a["start-position"]) {
				var title = a.type;
				if(a.preferredLabel) {
					title += ", " + a.preferredLabel;
				}
				var css = "annotation_no_highlight";
				if(isSelected(a)) {
					css = "annotation_highlight";
					if(this.firstSelected == null) {
						this.firstSelected = a;
					}
				}
				if(lastEnd < a["start-position"]) {
					res.push(<span 
						key={key++}
						id={"a_" + lastId}>
							{text.substring(lastEnd, a["start-position"])}
						</span>);
				}
				res.push(<span 
					key={key++} 
					className={css}
					id={"annotation_" + a["annotation-id"]}
					onPointerUp={this.onSelectAnnotationClick.bind(this, a)}
					title={title}>
						{text.substring(a["start-position"], a["end-position"])}
					</span>);
			}
			lastEnd = a["end-position"];
			lastId = a["annotation-id"];
		}
		if(lastEnd < text.length) {
			res.push(<span key={key++} id={"a_" + lastId}>{text.substring(lastEnd)}</span>);
		}
		return res;
	}

	renderText() {
		return (
			<div id="annotation_text" className="annotation_text">
				{this.buildText()}
			</div>
		);
	}

	renderAnnotationFilter() {
		return (
			<div className="annotation_filter">
				<div>
					<label htmlFor="annotation_filter_checkbox">Gruppera efter typ</label>
					<input 
						id="annotation_filter_checkbox"
						type="checkbox"
						value={this.state.isGroupingOnType}
						onChange={this.onGroupOnTypeChanged.bind(this)}/>
				</div>
				<input 
					placeholder="Filter..." 
					value={this.state.filter == null ? "" : this.state.filter}
					onChange={this.onFilterChanged.bind(this)}/>
			</div>
		);
	}

	renderAnnoationTags() {
		var isSelected = (item) => {
			return this.state.selectedId == item["annotation-id"];
		};
		/*
		var isTypeSelected = (type) => {
			return this.state.selectedTypes.includes(type);
		};
		var types = [];
		for(var i=0; i<this.state.data.annotations.length; ++i) {
			var a = this.state.data.annotations[i];
			if(!types[a.type]) {
				types[a.type] = 1;
			} else {
				types[a.type]++;
			}
		}
		var annotationTypes = [];
		var key = 0;
		var tmp = [];
		var t;
		for(t in types) {
			tmp.push({type: t, count: types[t]});
		}
		tmp.sort((a, b) => {return (a.type > b.type ? 1 : -1);});
		for(var i=0; i<tmp.length; ++i) {
			t = tmp[i];
			var css = isTypeSelected(t.type) ? "annotation_type selected" : "annotation_type";
			annotationTypes.push(
				<div 
					key={key++} 
					className={css}
					onPointerUp={this.onToggleTypeSelectedClicked.bind(this, t.type)}>
					<span>
						{t.type}
					</span>
				</div>
			);
		};
		*/

		// show all items
		var items = this.state.data.annotations.filter((item) => {
			return this.state.filter == null || item["matched-string"].indexOf(this.state.filter) != -1;
		});
		var listItems = [];
		if(this.state.isGroupingOnType) {
			for(var i=0; i<items.length; ++i) {
				var item = items[i];
				var entry = listItems.find((e) => {
					return e.type == item.type;
				});
				if(entry == null) {
					entry = {
						type: item.type,
						items: [],
					};
					listItems.push(entry);
				}
				entry.items.push(item);
			}
			listItems = listItems.map((item, index) => {
				var subItmes = item.items.map((subItem, subIndex) => {
					var css = isSelected(subItem) ? "annotation_type selected" : "annotation_type";
					return (
						<div 
							key={subIndex} 
							className={css}
							onPointerUp={this.onAnnotationClicked.bind(this, subItem)}>
							<span>
								{subItem["matched-string"]}
							</span>
						</div>
					);
				});
				var s = {
					marginLeft: 10,
				};
				return (
					<div key={index}>
						<div className="annotation_type_category">
							<span>{item.type}</span>
						</div>
						<div className="annotation_list" style={s}>{subItmes}</div>
					</div>
				);
			});
		} else {
			listItems = items.map((item, index) => {
				var css = isSelected(item) ? "annotation_type selected" : "annotation_type";
				return (
					<div 
						key={index} 
						className={css}
						onPointerUp={this.onAnnotationClicked.bind(this, item)}>
						<span>
							{item["matched-string"]}
						</span>
					</div>
				);
			});
		}
		return (
			<div className="annotation_types group">
				<h3>Annoteringar</h3>
				{this.renderAnnotationFilter()}
				<div className="annotation_list">{listItems}</div>
			</div>
		);
	}

	renderAnnotateContent() {
		return (
			<div className="annotation_container">
				<div className="button_row">
					<Button
						isEnabled={this.state.selectedText ? true : false}
						onClick={this.onCreateAnnotationClicked.bind(this, this.state.selectedText)}
						text="Ny annotering"/>
					<Button
						css="save"
						onClick={this.onSaveClicked.bind(this)}
						text="Spara"/>
				</div>
				<div className="annotation_main">
					<div className="annotation_result group">
						<h3>Dokument</h3>
						{this.renderText()}
					</div>
					{this.renderAnnoationTags()}
				</div>
			</div>
		);
	}

	renderContent() {
		if(this.state.data) {
			return this.renderAnnotateContent();
		} else if(this.state.loading) {
			return (
				<AnnotateLoading />
			);
	 	} else {
			return (
				<AnnotateChooseFile 
					onFromText={this.onFromText.bind(this)}
					onFromFile={this.onFromFile.bind(this)}/>
			);
		}
	}

	renderDialog() {
		if(!this.state.selected) {
			return;
		}
		return (
			<div className="edit_overlay">
				<div className="edit_dialog">
					<EditAnnotation
						newConnection={this.state.newConnection == true}
						annotation={this.state.selected}
						annotationTypes={this.types}						
						onAnnotationSaved={this.onAnnotationSaved.bind(this)}
						onAnnotationChanged={this.onAnnotationChanged.bind(this)}
						onAnnotationDeleted={this.onAnnotationDeleted.bind(this)}
						onCloseDialog={this.onHideDialog.bind(this)}
						/>
				</div>
			</div>
		);
	}

    render() {
        return (
			<div className="content">
				<h1 className="annotation_margin">Annotate</h1>			
				{this.renderContent()}
				{this.renderDialog()}
			</div>
        );
    }
	
}

export default Annotate