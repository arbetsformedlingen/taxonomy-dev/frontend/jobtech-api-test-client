import React from 'react';

class Button extends React.Component { 

	constructor() {
        super();
        // state
        this.state = {			
        };
		
    }

	onClick() {
		var enabled = this.props.isEnabled == null ? true : this.props.isEnabled;
		if(enabled && this.props.onClick) {
			this.props.onClick();
		}
	}

    render() {
		var css = (this.props.css ? this.props.css : "") + " button";
		var enabled = this.props.isEnabled == null ? true : this.props.isEnabled;
		if(!enabled) {
			css += " button_disabled";
		}		
        return (
			<div 
				className={css}
				
				onPointerUp={this.onClick.bind(this)}>
				{this.props.text}
			</div>
        );
    }
	
}

export default Button